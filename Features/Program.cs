﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Features
{
    class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<Employee> developers = new Employee[]
            {
                new Employee
                {
                    Id = 1, Name = "John"
                },
                new Employee
                {
                    Id = 2, Name = "Shawn"
                },
                new Employee
                {
                    Id = 3, Name = "Scott"
                },
                new Employee
                {
                    Id = 4, Name = "Serena"
                },
            };

            // beautiful thing about IEnumerable is - you can hide about any sort of data structure 
            // or operation behind the interface.



            IEnumerable<Employee> sales = new List<Employee>()
            {
                new Employee{Id = 13, Name = "Alexa"},

            };

            //foreach (var person in developers)
            //{
            //    Console.WriteLine($"{person.Id} : {person.Name}");
            //}

            //IEnumerator<Employee> enumerator = sales.GetEnumerator();

            //// Every time I can MoveNext(), I don't know if the underlying Enumerator 
            //// has to access a new element in an array, or index into next item of a list, 
            //// or follow a pointer of a tree or even make a call back to the database server to 
            //// fetch the next record out of the table. So IEnumerable is the perfect source for 
            //// hiding the source of the data. Thats why 98% of the LINQ features, the LINQ operators
            //// that can do things like ordering, filtering, counting, aggregating, they all can work against 
            //// IEnumerable of T. 
            //while (enumerator.MoveNext())
            //{
            //    Console.WriteLine(enumerator.Current.Name);
            //}

            //sales.GetEnumerator();
            //developers.GetEnumerator();

            //foreach (var employee in developers.Where(NameStartsWithS))
            //{
            //    Console.WriteLine(employee.Name);
            //}

            //Console.WriteLine(developers.Count());

            Func<Employee, string> upperSelector = str => str.Name.ToUpper();

            Func<Employee, string> upperSelectorFull = delegate(Employee emp)
            {
                return emp.Name.ToUpper();
            };


            Func<Employee, string> upperSelectorLE = emp =>
            {
                int temp = 0; 
                return emp.Name.ToUpper();
            };



            IEnumerable<string> empNames = developers.Select(upperSelectorLE);

            foreach (var e in empNames)
            {
                Console.WriteLine(e);
            }



            //As the name suggests, an anonymous method is a method without a name.Anonymous methods in C# can be defined using the delegate keyword and can be assigned to a variable of delegate type.


            var query1 = developers.Where(e => e.Name.Length == 5)
                .OrderBy(e => e.Name)
                .Select(e => e);

          


            //   var query2 = from developer in developers
            //               where developer.Name.Length == 5
            //               orderby developer.Name
            //               select developer;

            foreach (var employee in query1)
            {
                Console.WriteLine(employee.Name);
            }




            //           // sql query string 
            //string[] cities = { "Nashville", "Hyderbad", "London", "Los Angeles"};
            //           IEnumerable<string> filteredCities =
            //               from city in cities
            //               where city.StartsWith("L") && city.Length < 20
            //               orderby city
            //               select city;



        }



        //private static bool NameStartsWithS(Employee emp)
        //{
        //    return emp.Name.StartsWith("S");
        //}
    }
}
