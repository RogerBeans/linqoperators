﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqQueries
{
    public static class MyLinq
    {

        public static IEnumerable<T> Filter<T>( this IEnumerable<T> source, 
                                                Func<T,bool> predicate)
        {
            var result = new List<T>();

            foreach (var item in source)
            {
                if (predicate(item))
                {
                    //result.Add(item);

                    // yield provides us deferred execution (as lazy as possible)
                    // i.e. Query does no real work until we force the query to produce a result

                   yield return item;
                }
            }

            //return result;

        }
    }
}
