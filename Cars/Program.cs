﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Cars
{
    class Program
    {
        static void Main(string[] args)
        {
            var cars = ProcessFile("../../../fuel.csv");
            //foreach (var car in cars)
            //{
            //    Console.WriteLine(car.Name);
            //}


            // Complete each of these exercises using both 

            // 1) which car has the best fuel efficiency ?
            // use data related to Combined column for it.

            
            //2) Get 10 cars with the best fuel efficient in descending order


            //3) IF in (2), there are more than 1 car with same value for combined efficiency,
            // then sort the values based on alphabetical name of the car



            // 4) find the top "BMW" from 2016 that has the best fuel efficiency
            // a) use Take operator 
            // b) use First Operator 
            // c) what is the difference between the two in terms of execution of program?


            //d) DO not use Where operator and just use First operator 
            // Note: First can be used for filtering like Where operator

            // e) compare difference between (a) , (b) and (d) in terms of performance of execution

            

            // 5) FIND THE top "aaa" car 
            // NOTE: If you use First operator and the result is null, then a null exception will be thrown when you evoke a method or properties on the result.
            // HINT: use FirstOrDefault operator
            // Also Check: Last & LastOrDefault operator 


            // QUANTIFIERS Operators : if anything matches the predicate or 
            // the dataset contains a specific items. All of these operators return TRUE or FALSE
            //
            // NOTE: since the result is bool, these quantifiers do not provide deferred execution
            // e.g. Any operator, All operator, Contains operator 
            //6) Are there any manufactures named "Ford" ?

            //7) Do all of the manufactures have cars "Ford" ?

            //


            





            // PRINT result of query here 
            //foreach (var car in cars)
            //{

            //}


            Console.ReadKey();
        }

        private static List<Car> ProcessFile(string path)
        {
            return
                File.ReadAllLines(path)
                    .Skip(1) // skip and take operators are useful for pagination operations
                    .Where(line => line.Length > 1)
                    .Select(Car.ParseFromCsv)
                    .ToList();// to make sure we are not re-reading the file in the disk


            //var query = from line in File.ReadAllLines(path).Skip(1)
            //            where line.Length > 1
            //            select Car.ParseFromCsv(line);

            //return query.ToList();


        }
    }
}
