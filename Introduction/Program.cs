﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Introduction
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\windows";
            //ShowLargeFilesWithoutLinq(path);
            ShowLargeFilesWithLinq(path);
        }

        private static void ShowLargeFilesWithLinq(string path)
        {
            //writing a query 
            // less code and much more expressive 
            // looks like query you would write for SQL in RDBMS

            // this one looks like SQL 
            var query = from file in new DirectoryInfo(path).GetFiles()
                orderby file.Length descending
                select file;

            // this one looks like method call
            var queryMethod = new DirectoryInfo(path).GetFiles()
                .OrderByDescending(f => f.Length)
                .Take(5);



            foreach (var file in queryMethod)
            {
                Console.WriteLine($"{file.FullName,-30} : {file.Length,10:N0}");
            }
        }

        private static void ShowLargeFilesWithoutLinq(string path)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            FileInfo [] files  = directory.GetFiles();

            Array.Sort(files, new FileInfoComparer());

            //foreach(FileInfo file in files)
            //{
            //    Console.WriteLine($"{file.Name} : {file.Length}");
            //}

            for (int i = 0; i < 5; i++)
            {
                FileInfo file = files[i];
                Console.WriteLine($"{file.FullName, -30} : {file.Length, 10:N0}");

            }
        }
    }

    public class FileInfoComparer : IComparer<FileInfo>
    {
        public int Compare(FileInfo x, FileInfo y)
        {
            // you return -1 if the first file is less than 2nd file 
            // return 0 if equal
            // return 1 if the first file is greater than the 2nd one 
            return y.Length.CompareTo(x.Length);
        }
    }
}
